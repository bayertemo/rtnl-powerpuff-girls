import fetch from 'cross-fetch';
import { apiConfig } from './config'
import endpoints from './endpoints';
import { checkServerResponse } from './errors';

// define rest service methods
const restService = {
  api: {}
}

restService.get = (url, options = {}) =>
  fetch(url, options)
  .catch(err => {
    throw new Error(err)
  });

Object
  .keys(endpoints)
  .forEach(name => {
    const { url } = endpoints[name];
    restService.api[name] = (...params) =>
      fetch(`${apiConfig.base}${url(...params)}`)
        .then(checkServerResponse)
        .catch(err => {
          console.error('Error occurred:', err);
          throw err;
        })
    });

export default restService;