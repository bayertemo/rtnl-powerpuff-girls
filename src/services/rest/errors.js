export const checkServerResponse = response => {
  if (response.status >= 400 && response.status < 600) {
    throw Error(response.statusText);
  } else {
    return response.json();
  }
}
      