export default {
  getAllSHows: {
    url: () => '/shows'
  },
  getShowsByPage: {
    url: (page = 0) => `/shows?page=${page}`
  },
  getShowDetails: {
    url: (id) => `/shows/${id}`
  },
  getShowSeasons: {
    url: (id) => `/shows/${id}/seasons`
  },
  getEpisodes: {
    url: (id) => `/shows/${id}/episodes`
  },
  getEpisodeDetails: {
    url: (id, season, number) => `/shows/${id}/episodebynumber?season=${season}&number=${number}`
  }
}
