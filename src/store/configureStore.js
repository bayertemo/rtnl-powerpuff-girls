import { applyMiddleware, compose, createStore, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import shows from '../components/Shows/reducers';
import selectedShowDetails from '../components/ShowDetailsPage/reducers';
import selectedShowEpisodeDetails from '../components/ShowEpisodesPage/reducers';

const reducers = combineReducers({
  shows,
  selectedShowDetails,
  selectedShowEpisodeDetails
});

export default function configureStore() {

  const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) :
      compose

  const middleware = [
    thunk
  ];

  const enhancer = composeEnhancers(
    applyMiddleware(...middleware)
  );

  return createStore(reducers, enhancer);
}