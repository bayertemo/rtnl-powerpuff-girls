import { createSelector } from 'reselect';
import groupBy from 'lodash/groupBy';

const getRawShowDetails = ({
  selectedShowDetails: {
    rawItemInfo = {},
    rawItemSeasons = [],
    rawItemEpisodes = [] }
  }) => ({ rawItemInfo, rawItemSeasons, rawItemEpisodes });

// Any additional modification to be done here
export default createSelector(
  [
    getRawShowDetails
  ],
  ({ rawItemInfo, rawItemEpisodes, rawItemSeasons }) => {

    const showEpisodes = rawItemEpisodes.map(episode => ({
      showId: rawItemInfo.id,
      episodeDetailsUrl: `/episode/${rawItemInfo.id}/${episode.season}/${episode.number}`,
      ...episode
    }));

    const grouped = groupBy(showEpisodes, 'season');

    const showSeasons = rawItemSeasons.map(season => ({
      ...season,
      name: season.name || `Season ${season.number}`,
    }));

    const groupedShowEpisodes = Object.keys(grouped).map(key => ({
      season: showSeasons.find(season => season.number === Number(key)) || {},
      episodes: grouped[key]
    }));

    return {
    showInfo: rawItemInfo,
    showSeasons,
    showEpisodes: groupedShowEpisodes
  };
})
