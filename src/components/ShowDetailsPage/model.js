export const initialState = {
  loading: false,
  error: undefined,
  rawItemInfo: {},
  rawItemEpisodes: [],
  rawItemSeasons: [],
}