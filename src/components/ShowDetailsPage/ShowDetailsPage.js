import React, { Fragment } from 'react';
import styled from 'styled-components';
import chunk from 'lodash/chunk';
import { Theme, Media } from '../common';
import { Row } from '../common/LayOut';
import { Col3 } from '../common/Columns';
import { EpisodesQuickAccess } from './components';

const Main = ({ showSeasons = [], showEpisodes = [], showInfo: { id, name, summary, image: { medium } = { } } = {} }) =>
  <Fragment>
    {
      medium &&
        <ImageWrapper>
          <Image src={ medium } />
        </ImageWrapper>
    }
    <InfoWrapper>
      <h1>{ name }</h1>
      <div dangerouslySetInnerHTML={{ __html: summary }} />
      <EpisodeWrapper>
      { chunk(showEpisodes, 3).map((episodesChunk, index) =>
        <Row key={ index }>
          {
            episodesChunk.map(({ season: { name, image } = {}, episodes }, index) =>
              <EpisodeCol key={ index }>
                {
                  name && <h3>{ name }</h3>
                }
                {
                  image && <Image src={ image.medium } />
                }
                <EpisodesQuickAccess options={ episodes } />
              </EpisodeCol>)
          }
        </Row>)
      }
      </EpisodeWrapper>
    </InfoWrapper>
  </Fragment>

export default Main;

const ImageWrapper = styled.div`
  ${Media.desktop`
      float: left;
      width: 220px;
      padding-right: 25px; 
  `}
`

const InfoWrapper = styled.div`

  ${Media.desktop`
      float: left;
      width: calc(100% - 220px);
  `}

`

const EpisodeWrapper = styled.div`
  margin: 0 -10px;
`

const EpisodeCol = styled(Col3)`
  padding: 10px;
`

const Image = styled.img`
  width: 100%;
  height: auto;
  border: 1px solid ${Theme.colors.dark_green};
`;