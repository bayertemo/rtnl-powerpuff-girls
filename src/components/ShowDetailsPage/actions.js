import { createAction } from 'redux-actions';
import { rest } from '../../services';

export const setDataLoadingFlag = createAction('SHOW_DETAILS/SET_LOADING_FLAG');
export const setError = createAction('SHOW_DETAILS/SET_ERROR');
export const setData = createAction('SHOW_DETAILS/SET_DATA');

export const fetchShowDetails = showId => dispatch => {
  dispatch(setDataLoadingFlag());

  Promise.all([
    rest.api.getShowDetails(showId),
    rest.api.getShowSeasons(showId),
    rest.api.getEpisodes(showId)
  ])
  .then(([ showInfo, showSeasons, showEpisodes ]) => 
    dispatch(setData({ showInfo, showSeasons, showEpisodes }))
  )
  .catch(error => dispatch(setError({ error })));
}
