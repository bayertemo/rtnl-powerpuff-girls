import { handleActions } from 'redux-actions';
import { initialState } from './model';
import { setError, setData, setDataLoadingFlag } from './actions';

const handlerDataLoading = state => ({
  ...state,
  error: undefined,
  loading: true,
  rawItemInfo: {},
  rawItemEpisodes: []
});

const handleSetError = (state, { payload: { error } }) => ({
  ...state,
  error,
  loading: false
});

const handleSetData = (state, { payload: { showInfo, showEpisodes, showSeasons }}) => ({
  ...state,
  rawItemInfo: showInfo,
  rawItemEpisodes: showEpisodes,
  rawItemSeasons: showSeasons,
  loading: false,
  error: undefined
});

export default handleActions({
  [setDataLoadingFlag]: handlerDataLoading,
  [setError]: handleSetError,
  [setData]: handleSetData
}, initialState)
