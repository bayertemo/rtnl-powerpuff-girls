import React, { Fragment } from 'react';
import styled from 'styled-components';
import { Theme, Link } from '../../../common';

const TopMenu = ({ summary, options = [] }) =>
  <Fragment>
    {
      summary && <p> { summary } </p>
    }
    <ul>
      {
        options.map(({ name, episodeDetailsUrl }, index) =>
        <StyledLink key={ index } to={ episodeDetailsUrl } >
          <StyledLi >{ name }</StyledLi>
        </StyledLink>)
      }
    </ul>
  </Fragment>

export default TopMenu;

const StyledLi = styled.li`
  display: block;
  padding: 5px;

  & + & {
    margin-top: 10px;
  }

  &:hover {
    background-color: ${Theme.colors.off_white};
    text-decoration: none;
  }
`;

const StyledLink = styled(Link)`
  color: ${Theme.colors.black};
  font-size: 14px;

`