import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux'
import { compose, lifecycle } from 'recompose';
import * as actions from './actions'
import ShowDetailsPage  from './ShowDetailsPage';
import selectors from './selectors'

const withDataLoad = lifecycle({
  componentDidMount() {
    const { fetchShowDetails, match: { params: { id } = {} } = {}} = this.props;
    fetchShowDetails(id);
  },
  componentWillReceiveProps(newProps) {
    const { fetchShowDetails, match: { params: { id } = {} } = {}} = this.props;
    const { match: { params: { id: newId } = {} } = {}} = newProps;
    if (id !== newId ) {
      fetchShowDetails(newId);
    }
  }
});

export default compose(
  connect(selectors, actions),
  withRouter,
  withDataLoad
)(ShowDetailsPage);
