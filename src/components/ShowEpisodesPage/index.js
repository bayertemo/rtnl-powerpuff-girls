import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux'
import { compose, lifecycle } from 'recompose';
import * as actions from './actions'
import ShowEpisodesPage  from './ShowEpisodesPage';
import selectors from './selectors'

const withDataLoad = lifecycle({
  componentDidMount() {
    const { fetchShowDetails, match: { params: { id, number, season } = { }} = { }} = this.props;
    fetchShowDetails(id, season, number);
  },
  componentWillReceiveProps(newProps) {
    const { fetchShowDetails, match: { params: { id, number, season } = { }} = { }} = this.props;
    const { match: { params: { id: newId , number: newNumber, season: newSeason } = { }} = { }} = newProps;
    if (id !== newId || number !== newNumber || season !== newSeason ) {
      fetchShowDetails(newId, newSeason, newNumber);
    }
  }
});

export default compose(
  connect(selectors, actions),
  withRouter,
  withDataLoad
)(ShowEpisodesPage);
