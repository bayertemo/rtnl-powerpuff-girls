import { createSelector } from 'reselect';

const getRawShowDetails = ({
  selectedShowEpisodeDetails: {
    rawItemInfo = {},
    rawEpisodeInfo = {} }
  }) => ({ rawItemInfo, rawEpisodeInfo });

// Any additional modification to be done here
export default createSelector(
  [
    getRawShowDetails
  ],
  ({ rawItemInfo, rawEpisodeInfo }) => ({
    showInfo: {
      showDetailsUrl: `/show-details/${rawItemInfo.id}`,
      ...rawItemInfo
    },
    episodeInfo: rawEpisodeInfo
  })
)
