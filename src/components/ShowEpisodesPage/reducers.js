import { handleActions } from 'redux-actions';
import { initialState } from './model';
import { setError, setData, setDataLoadingFlag } from './actions';

const handlerDataLoading = state => ({
  ...state,
  error: undefined,
  loading: true,
  rawItemInfo: {},
  rawEpisodeInfo: {},
});

const handleSetError = (state, { payload: { error } }) => ({
  ...state,
  error,
  loading: false
});

const handleSetData = (state, { payload: { showInfo, episodeInfo }}) => ({
  ...state,
  rawItemInfo: showInfo,
  rawEpisodeInfo: episodeInfo,
  loading: false,
  error: undefined
});

export default handleActions({
  [setDataLoadingFlag]: handlerDataLoading,
  [setError]: handleSetError,
  [setData]: handleSetData
}, initialState)
