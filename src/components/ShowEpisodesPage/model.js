export const initialState = {
  loading: false,
  error: undefined,
  rawItemInfo: {},
  rawEpisodeInfo: {}
}