import React, { Fragment } from 'react';
import styled from 'styled-components';
import { Theme, Media, Link } from '../common';

const Main = ({
  episodeInfo: { name: episodeName, summary: episodeSummary, image } = {},
  showInfo: { id, name, summary, showDetailsUrl } = {}
}) =>
  <Fragment>
    {
      image && image.medium &&
        <ImageWrapper>
          <Image src={ image.medium } />
        </ImageWrapper>
    }
    <InfoWrapper>
      <StyledLink to={ showDetailsUrl }>{ name }</StyledLink>
      <h1>{ episodeName } </h1>
      <div dangerouslySetInnerHTML={{ __html: episodeSummary }} />
    </InfoWrapper>
  </Fragment>

export default Main;

const ImageWrapper = styled.div`
  ${Media.desktop`
      float: left;
      width: 220px;
      padding-right: 25px; 
  `}
`

const InfoWrapper = styled.div`

  ${Media.desktop`
      float: left;
      width: calc(100% - 220px);
  `}

`

const Image = styled.img`
  width: 100%;
  height: auto;
  border: 1px solid ${Theme.colors.dark_green};
`;

const StyledLink = styled(Link)`
  color: ${Theme.colors.black};
  font-size: 12px;
  text-decoration: none;

  &:hover {
    text-decoration: underline;
  }
`