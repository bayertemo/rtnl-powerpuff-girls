import { createAction } from 'redux-actions';
import { rest } from '../../services';

export const setDataLoadingFlag = createAction('SHOW_EPISODES/SET_LOADING_FLAG');
export const setError = createAction('SHOW_EPISODES/SET_ERROR');
export const setData = createAction('SHOW_EPISODES/SET_DATA');

export const fetchShowDetails = (showId, episodeId, episodeNumber) => dispatch => {
  dispatch(setDataLoadingFlag());

  Promise.all([
    rest.api.getShowDetails(showId),
    rest.api.getEpisodeDetails(showId, episodeId, episodeNumber)
  ])
  .then(([showInfo, episodeInfo]) => dispatch(setData({ showInfo, episodeInfo })))
  .catch(error => dispatch(setError({ error })));
}
