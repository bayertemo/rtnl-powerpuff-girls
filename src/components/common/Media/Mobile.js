import React from 'react';
import Responsive from 'react-responsive';
import { MOBILE_EDGE } from './constants';

export default ({ children }) =>
  <Responsive maxWidth={ MOBILE_EDGE }>{ children }</Responsive>