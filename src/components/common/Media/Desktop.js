import React from 'react';
import Responsive from 'react-responsive';
import { MOBILE_EDGE } from './constants';

export default ({ children }) =>
  <Responsive minWidth={ MOBILE_EDGE + 1 }>{ children }</Responsive>