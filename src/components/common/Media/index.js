
import { default as Desktop } from './Desktop';
import { default as Mobile } from './Mobile';
import { desktop, tablet, mobile } from './mixins';

export default {
  Desktop,
  Mobile,
  desktop,
  tablet,
  mobile
};