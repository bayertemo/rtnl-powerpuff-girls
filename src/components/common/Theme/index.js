const colors = {
  // shades
  black: '#3F3F3F', // primary text
  off_white: '#f6f6f6', // backgrounds
  white: '#ffffff',
  dark_green: '#1f4b47',
  green: '#3C948B',

  //
  transparent_overlay: 'rgba(0, 0, 0, 0.5)'
}

export default {
  colors
}
