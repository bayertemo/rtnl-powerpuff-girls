import React from 'react';
import styled from 'styled-components';
import { Theme } from '../';
import { Container} from '../LayOut';
import { Logo, TopMenu } from './components';

const Main = ({ showId }) =>
  <React.Fragment>
    <LogoWrapper>
      <Container>
        <StyledLogo />
      </Container>
    </LogoWrapper>
    <GreenMenuWrapper>
      <Container>
        <TopMenu />
      </Container>
    </GreenMenuWrapper>
  </React.Fragment>  

export default Main;

const LogoWrapper = styled.div`
  background-color: ${Theme.colors.black};
`

const StyledLogo = styled(Logo)`
  margin-left: 10px;
`

const GreenMenuWrapper = styled.div`
  background-color: ${Theme.colors.green};
`