import React from 'react';
import styled from 'styled-components';
import { Link } from '../../../';
import logo from './assets/logo.png';

const Logo = ({ className }) =>
  <Home className={ className } to='/' >
    <Image src={ logo } />
  </Home>

export default Logo;

const Home = styled(Link)`
  display: inline-block;
  padding: 10px 0;
`

const Image = styled.img`
  width: 158px;
  height: 50px;
`