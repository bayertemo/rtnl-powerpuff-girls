import React, { Fragment } from 'react';
import styled from 'styled-components';
import { Theme, Link } from '../../../';

// Menu could be hosted as plain json and loaded when component load using service call
// So the menu could be managed without having to rebuild the entire app
// But kept inline for timesaving
const TopMenu = () =>
  <Fragment>
    <ul>
      <StyledLi><Featured>Popular:</Featured></StyledLi>
      <StyledLi><StyledLink to={ '/show-details/6771' }>The Powerpuff Girls</StyledLink></StyledLi>
    </ul>
  </Fragment>

export default TopMenu;

const Featured = styled.span`
  color: ${Theme.colors.white};
  font-weight: 600;
  margin: 0 14px;
`

const StyledLi = styled.li`
  display: inline-block;
`;

const StyledLink = styled(Link)`
  padding: 15px;
  display: block;
  color: ${Theme.colors.white};
  margin-right: 14px;
  &:hover {
    background-color: ${Theme.colors.black};
    text-decoration: none;
  }
  
`