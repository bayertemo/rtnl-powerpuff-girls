import React from 'react';
import styled from 'styled-components';
import Theme from '../Theme';
import Mixin from '../Mixin';
import loading from './assets/oval.svg';

const Spinner = () =>
  <Wrapper>
    <SpinnerIcon src={loading} alt="loading..." />
  </Wrapper>

export default Spinner

const Wrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: ${Theme.colors.transparent_overlay};
  z-index: 1200;
`

const SpinnerIcon = styled.img`
  position: absolute;
  top: calc(50% - 30px);
  left: calc(50%  - 30px);
  animation: 0 1.7s linear infinite;
  transform-origin: center;
  ${Mixin.responsive('width', '100px', '60px')}
  ${Mixin.responsive('height', '100px', '60px')}
`