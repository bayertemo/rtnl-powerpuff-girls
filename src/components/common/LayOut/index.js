import styled from 'styled-components';
import { clearFix } from 'polished';
import { FLUID_CONTAINER_WIDTH } from './constants';
import Media from '../Media';

export const Container = styled.div`
  margin: 0 auto;
  ${Media.desktop`
    width: ${FLUID_CONTAINER_WIDTH};
  `};
`

export const Row = styled.div`
  ${Media.desktop`
    ${clearFix()};
  `}
`