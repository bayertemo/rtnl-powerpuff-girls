import React from 'react';
import { NavLink } from 'react-router-dom'
import styled from 'styled-components';

export const A = styled.a`
  text-decoration: none;

  &:hover {
    text-decoration: underline;
  }
`

export const Link = ({ className, children, to = '/', ...props }) => 
  <NavLink { ...props } to={ to } className={ className }>
    { children }
  </NavLink>