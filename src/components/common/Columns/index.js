import styled from 'styled-components';
import Media from '../Media';

export const Col3 = styled.div`
  ${Media.desktop`
    float: left;
    position: relative;
    width: 33.3333%;
  `}
`
export const Col6 = styled(Col3)`
  ${Media.desktop`
    width: 50%
  `}
`;

export const Col9 = styled(Col3)`
  ${Media.desktop`
    width: 66.6666%
  `}
`;
