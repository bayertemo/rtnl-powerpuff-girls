import styled from 'styled-components';

export const Error = styled.div`
  padding: 20px;
  text-align: center;
  font-size: 16px;
`