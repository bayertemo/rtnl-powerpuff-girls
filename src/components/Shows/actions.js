import { createAction } from 'redux-actions';
import { rest } from '../../services';

export const setDataLoadingFlag = createAction('SHOWS/SET_LOADING_FLAG');
export const setError = createAction('SHOWS/SET_ERROR');
export const setData = createAction('SHOWS/SET_DATA');

export const fetchShows = page => async dispatch => {
  dispatch(setDataLoadingFlag());

  try {
    const data = await rest.api.getShowsByPage(page);

    dispatch(setData({ data }));
  } catch (error) {

    dispatch(setError({ error }))
  }
}