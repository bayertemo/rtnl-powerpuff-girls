import React from 'react';
import styled from 'styled-components';
import { Theme, Link } from '../../../common';
import star from './assets/star.png';

const ShowBrief = ({ className, showData: { name, rating: { average }, image, detailsUrl } = {}}) =>
  <StyledLink to={ detailsUrl }>
    <div className={ className }>
      {
        image && image.medium && <Image src={ image.medium } />
      }
      <Header>{ name}</Header>
      {
        average && <p><Stars /> { average }</p>
      }
    </div>
  </StyledLink>

export default ShowBrief;

const StyledLink = styled(Link)`
  color: ${Theme.colors.black};
`

const Header = styled.h2`
  font-weight: 600;    
`

const Image = styled.img`
  width: 100%;
  height: auto;
  border: 1px solid ${Theme.colors.dark_green};
`;

const Stars = styled.span`
  display: inline-block;
  width: 15px;
  height: 15px;
  content: url(${star});
  }
`
