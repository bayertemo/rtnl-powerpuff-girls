import { connect } from 'react-redux'
import { compose, lifecycle } from 'recompose';
import * as actions from './actions'
import Shows from './Shows';
import selectors from './selectors'

const withDataLoad = lifecycle({
  componentDidMount() {
    const { fetchShows, page = 0 } = this.props;
    fetchShows(page);
  }
});

export default compose(
  connect(selectors, actions),
  withDataLoad
)(Shows);
