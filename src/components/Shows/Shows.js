import React, { Fragment } from 'react';
import styled from 'styled-components';
import { Theme, Mixin, Media } from '../common';
import { ShowBrief } from './components/ShowBrief';

const Shows = ({ showsSorted }) =>
  <Fragment>
    <Wrapper>
      {
        showsSorted
          .map( (data, index) => <StyledShowBrief key={ index } showData={ data } />)
      }
    </Wrapper>
  </Fragment>

export default Shows;

const Wrapper = styled.div`
  margin: 0 -10px;
  ${Mixin.responsive('padding', '0px', '0 10px;')};
`

const StyledShowBrief = styled(ShowBrief)`
  padding: 15px;
  ${Mixin.responsive('margin', '5px', '15px 0')};
  ${Media.desktop`
    display: inline-block;
    width: 230px;
    height: 400px;
    overflow-y: auto;
  `};
  border: 1px solid ${Theme.colors.off_white};
  background-color: ${Theme.colors.green};
`