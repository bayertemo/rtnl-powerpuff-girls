import { createSelector } from 'reselect';

const getRawShows = ({ shows: { rawShowsList = []}}) => rawShowsList;
const getSortKey = ({ sortKey }) => sortKey;

export default createSelector(
  [
    getRawShows,
    getSortKey
  ],
  (rawShowsList, sortKey) => ({
    showsSorted: rawShowsList.sort((a, b) => {
      if (!a[sortKey] || !b[sortKey]) {
        return 0;
      }
      if (a[sortKey] > b[sortKey]) {
        return -1;
      }
      if (a[sortKey] < b[sortKey]) {
        return 0;
      }
      return 0;
    }).map(({id, ...show}) => ({
        detailsUrl: `/show-details/${id}`,
        ...show
      })
    )
  })
)
