export const initialState = {
  loading: false,
  error: undefined,
  rawShowsList: [],
  sortKey: 'ratings'
}