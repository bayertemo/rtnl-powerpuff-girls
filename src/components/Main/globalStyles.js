import { injectGlobal } from 'styled-components'
import { normalize } from 'polished'
import { Theme } from '../common'

injectGlobal`
  ${normalize()};

  * {
    box-sizing: border-box;
    font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
  }

  h1 {
    font-size: 18px;
  }

  h2, p {
    font-size: 14px;
  }

  html {
    color: ${Theme.colors.black};
  }

  b,
  strong {
    font-weight: bold;
  }

  ul, li {
    padding: 0;
    margin: 0;
  }
`