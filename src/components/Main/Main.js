import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';
import styled from 'styled-components';
import { Mixin, PageHeader, Spinner, Error } from '../common';
import { Container, Row } from '../common/LayOut';
import { default as Shows } from '../Shows';
import { default as ShowDetailsPage } from '../ShowDetailsPage';
import { default as ShowEpisodesPage } from '../ShowEpisodesPage';
import { ERROR_GENERIC_MSG } from '../../constants';
import './globalStyles';


// Place for switching dev/prod application routes
const Main = ({ isLoading, hasError }) =>
  <Fragment>
    <PageHeader />
    <PaddedContainer>
      <Row>
        <Switch>
          <Route path="/episode/:id/:season/:number" component={ ShowEpisodesPage } />
          <Route path="/show-details/:id" component={ ShowDetailsPage } />
          <Route exact path='/' component={ Shows } />
        </Switch>
      </Row>
    </PaddedContainer>
    {
      isLoading &&
        <Spinner />
    }
    { hasError &&
      <Error>{ ERROR_GENERIC_MSG }</Error> 
    }
  </Fragment>

export default Main;

const PaddedContainer = styled(Container)`
  ${Mixin.responsive('padding', '15px 0 0 0', '15px')};
`

