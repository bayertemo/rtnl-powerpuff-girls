import { createSelector } from 'reselect';

const getShowsLoadingFlags = ({ shows: { loading, error }}) => ({ loading, error });
const getEpisodesLoadingFlags = ({ selectedShowEpisodeDetails: { loading, error }}) => ({ loading, error });
const getShowDetailsLoadingFlags = ({ selectedShowDetails: { loading, error }}) => ({ loading, error });

export default createSelector(
  [
    getShowsLoadingFlags,
    getEpisodesLoadingFlags,
    getShowDetailsLoadingFlags
  ],
  (...flags) => ({
    isLoading: flags.some(({ loading }) => loading),
    hasError: !flags.some(({ loading }) => loading) && flags.some(({ error }) => error)
  })
)