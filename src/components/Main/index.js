import { connect } from 'react-redux'
import selectors from './selectors'
import Main from './Main';

export default connect(selectors)(Main);
