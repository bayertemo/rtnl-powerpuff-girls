import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom'
import { Root } from './components/Root';
import configureStore from './store/configureStore';

const store = configureStore();

const App = () =>
  <Provider store={ store }>
    <BrowserRouter>
      <Root />
    </BrowserRouter>
  </Provider>  

export default App;
